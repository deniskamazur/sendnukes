import torch 
from .models import InferSent

from typing import List

class InferSentWrapper:
    def __init__(self, model_path='encoder/infersent2.pkl', model_params = None, w2v_path='dataset/fastText/crawl-300d-2M.vec'):
        self.model_path = model_path
        
        self.params = model_params or {'bsize': 64, 'word_emb_dim': 300, 'enc_lstm_dim': 2048,
                'pool_type': 'max', 'dpout_model': 0.0, 'version': 2}
        
        self.model = InferSent(self.params)
        self.model.load_state_dict(torch.load(self.model_path))
        
        self.model.set_w2v_path(w2v_path)
        self.model.build_vocab_k_words(K=100000)
        
    def similarity(self, sentence1, sentence2):
        vec1 = self.model.encode([sentence1])
        vec2 = self.model.encode([sentence2])
        
        return torch.nn.functional.cosine_similarity(torch.tensor(vec1), torch.tensor(vec2), dim=-1)

    def get_most_relevant(self, features: List[str], question: str):
        features = [(feature, self.similarity(feature, question)) for feature in features]
        return sorted(features, key=lambda x: x[1], reverse=True)

    def answer(self, features: List[str], question: str):
        return self.get_most_relevant(features, question)[0][0]
