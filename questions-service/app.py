from flask import Flask, jsonify, request, abort
from infersent.wrapper import InferSentWrapper

app = Flask(__name__)

infersent = InferSentWrapper(model_path="infersent/encoder/infersent2.pkl", w2v_path="infersent/dataset/fastText/crawl-300d-2M.vec")

@app.route('/question', methods=['POST'])
def question():
    if not request.json:
        abort(400)

    info = request.json

    data = {'features': [], 'question': info['question']}

    for features in info['drug'].keys():
        for feature in info['drug'][features]:
            data['features'].append(feature)

    answer = infersent.answer(data['features'], data['question'])
    return jsonify({'answer': answer})


if __name__ == '__main__':
    app.run(port=9999)

