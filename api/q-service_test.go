package main

import "testing"

func TestAnswerQuestion(t *testing.T) {
	drugInfo := DrugInfo{
		Name:             "xanax",
		Contradictions:   []string{"it gives you cancer"},
		ActiveSubstances: []string{},
		SideEffects:      []string{},
		Interactions:     []string{},
	}

	t.Log("requesting q-service")

	answer, err := asnwerQuestion(drugInfo, "does xanax give you cancer?")
	if err != nil {
		t.Error(err)
		return
	}

	t.Log(answer)
}
