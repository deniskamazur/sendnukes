package main

import (
	"time"
)

// DrugInfo describes basic drug propperties
type DrugInfo struct {
	// unique
	Name             string   `json:"name" bson:"name"`
	ActiveSubstances []string `json:"active_substances"`
	PicURL           string   `json:"photo_url"`
	Contradictions   []string `json:"contradictions"` // specific situation in which a drug, procedure, or surgery should not be used because it may be harmful.
	SideEffects      []string `json:"side_effects"`
	Interactions     []string `json:"interactions"` // Interactions with other drugs
}

// Drug stores
type Drug struct {
	Name     string      `json:"name"`
	Schedule []time.Time `json:"time"`
}

// User describes basic user data
type User struct {
	Name             string `json:"name"`
	TelegramID       string `json:"telegram_id"`
	TimeZone         *time.Location
	PrescriptedDrugs []Drug `json:"perscripted_drugs"`
}
