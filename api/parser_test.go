package main

import "testing"

func TestParseDrugUrl(t *testing.T) {
	t.Log("Testing searching for 'Nurofen'")
	res, err := ParseDrugUrl("Nurofen")
	if err != nil {
		t.Error("err is not nil: '" + err.Error() + "'.")
	}
	if res != "www.drugs.com/uk/nurofen-200mg-tablets-leaflet.html" {
		t.Error("ParseDrugUrl returned wrong url: " + res)
	}
}

func TestParseDrugInfo(t *testing.T) {
	url := "https://www.drugs.com/adderall.html"
	res, _ := parseDrugInfo(url)
	if res.Name != "Adderall" || res.SideEffects[0] != "Bladder pain" || len(res.Interactions) != 13 {
		t.Error("Wrong drug info for adderall")
	}

	url = "http://www.drugs.com/uk/nurofen-200mg-tablets-leaflet.html"
	res, _ = parseDrugInfo(url)
	if res.Name != "NUROFEN 200MG TABLETS" {
		t.Error("Wrong drug info for nurofen")
	}

	url = "https://www.drugs.com/mtm/ketamine.html"
	res, _ = parseDrugInfo(url)
	if res.SideEffects[len(res.SideEffects)-1] != "unusual tiredness or weakness" {
		t.Error("Wrong drug info for ketamine")
	}

	url = "https://www.drugs.com/ibuprofen.html"
	res, _ = parseDrugInfo(url)
	if res.Interactions[0] != "Asthma" {
		t.Error("Wrong drug info for ibuprofen")
	}
}
