package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

const (
	port = ":8000"
)

var (
	headersOk = handlers.AllowedHeaders([]string{"X-Requested-With"})
	originsOk = handlers.AllowedOrigins([]string{os.Getenv("ORIGIN_ALLOWED")})
	methodsOk = handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})
)

func main() {
	router := mux.NewRouter()

	// router.Handle("/api/drugs/info", http.HandlerFunc(handleGetDrugInfo)).Methods("GET")
	router.Handle("/api/drugs/getInfo", http.HandlerFunc(handleProductInfo)).Methods("POST")

	router.Handle("/api/question", http.HandlerFunc(handleQuestions)).Methods("GET")

	log.Printf("API is up and running in port %s", port)
	http.ListenAndServe(port, handlers.CORS(originsOk, headersOk, methodsOk)(router))
}
