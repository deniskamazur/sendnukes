package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"
)

func handleProductInfo(w http.ResponseWriter, r *http.Request) {
	var imageBuffer bytes.Buffer

	file, header, err := r.FormFile("image")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	defer file.Close()

	log.Printf("Received file %s\n", header.Filename)

	io.Copy(&imageBuffer, file)

	imageAnnotation, err := detectText(imageBuffer.Bytes())

	if err != nil || imageAnnotation == "" {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	productName := strings.ToLower(strings.Split(imageAnnotation, "\n")[0])
	drugLink, err := ParseDrugUrl(productName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		log.Fatal(err)
		return
	}

	info, err := parseDrugInfo(drugLink)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Fatal(err)
		return
	}

	infoBytes, err := json.Marshal(*info)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Fatal(err)
		return
	}

	fmt.Fprintf(w, string(infoBytes))
}

func handleQuestions(w http.ResponseWriter, r *http.Request) {
	// get data from request
	var request struct {
		ProductName string `json:"product_name"`
		Question    string `json:"question"`
	}

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// parse drug
	drugLink, err := ParseDrugUrl(request.ProductName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		log.Fatal(err)
		return
	}

	info, err := parseDrugInfo(drugLink)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Fatal(err)
		return
	}

	answer, err := asnwerQuestion(*info, request.Question)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	var response struct {
		Answer string `json:"answer"`
	}
	response.Answer = answer

	reponseBytes, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	fmt.Fprintf(w, string(reponseBytes))
}
