package main

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type BadStatusError struct {
	Message string
}

func (bse *BadStatusError) Error() string {
	return bse.Message
}

func asnwerQuestion(drugInfo DrugInfo, question string) (string, error) {
	var data struct {
		DrugInfo DrugInfo `json:"drug"`
		Question string   `json:"question"`
	}

	data.DrugInfo = drugInfo
	data.Question = question

	b := new(bytes.Buffer)
	json.NewEncoder(b).Encode(data)

	response, err := http.Post("http://localhost:9999/question", "application/json; charset=utf-8", b)

	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	var responseData struct {
		Answer string `json:"answer"`
	}

	if response.StatusCode != http.StatusOK {
		bytes, _ := ioutil.ReadAll(response.Body)
		return "", &BadStatusError{string(bytes)}
	}

	if err := json.NewDecoder(response.Body).Decode(&responseData); err != nil {
		return "", err
	}

	return responseData.Answer, nil
}
