package main

import (
	"net/http"

	"github.com/gorilla/context"
	"github.com/gorilla/sessions"

	mgo "gopkg.in/mgo.v2"
)

// Adapter is an http adapter
type Adapter func(http.Handler) http.Handler

// Adapt adapts a http handler
func Adapt(h http.Handler, adapters ...Adapter) http.Handler {
	for _, adapter := range adapters {
		h = adapter(h)
	}
	return h
}

func withDB(db *mgo.Session) Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			dbsession := db.Copy()
			defer dbsession.Close()
			context.Set(r, "database", dbsession)
			h.ServeHTTP(w, r)
		})
	}
}

func withAccess(store sessions.CookieStore, onDenie http.HandlerFunc) Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			session, err := store.Get(r, "rmonitor")

			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			if hasAccess, ok := session.Values["access"]; ok {
				if hasAccess.(bool) {
					h.ServeHTTP(w, r)
					return
				}
			}

			onDenie.ServeHTTP(w, r)
		})
	}
}

func withSession(store sessions.CookieStore) Adapter {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			session, _ := store.Get(r, "rmonitor")
			context.Set(r, "session", session)
			h.ServeHTTP(w, r)
		})
	}
}
