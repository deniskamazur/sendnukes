package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

const drugsUrl = "http://www.drugs.com"

type DrugNotFoundError struct {
	DrugName string
}

func (e DrugNotFoundError) Error() string {
	return "No drug with name " + e.DrugName + "."
}

type StatusCodeError struct {
	StatusCode int
	Status     string
}

func (e StatusCodeError) Error() string {
	return fmt.Sprintf("status code error: %d %s", e.StatusCode, e.Status)
}

func getDocumentFromUrl(url string) (doc *goquery.Document, err error) {
	if !strings.HasPrefix(url, "http") {
		url = "http://" + url
	}

	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, StatusCodeError{res.StatusCode, res.Status}
	}

	return goquery.NewDocumentFromReader(res.Body)
}

func ParseDrugUrl(name string) (drugName string, err error) {
	doc, err := getDocumentFromUrl(drugsUrl + "/search.php?searchterm=" + name)
	if err != nil {
		log.Fatal(err)
	}

	firstUrl := doc.Find(".search-result-source").First().Text()
	if firstUrl == "" {
		return "", DrugNotFoundError{DrugName: name}
	}
	return firstUrl, nil
}

func parseInteractions(interactionsUrl string) []string {
	var interactions []string = nil

	interactionsDoc, err := getDocumentFromUrl(drugsUrl + interactionsUrl)

	if err != nil {
		return nil
	}

	diseaseInteractionsUrl, exists := interactionsDoc.Find("a:contains('Disease Interactions')").First().Attr("href")
	if exists {
		diseaseInteractionsDoc, err := getDocumentFromUrl(drugsUrl + diseaseInteractionsUrl)
		if err == nil {
			diseaseInteractionsDoc.Find(".interactions").First().Find("li").Each(func(_ int, s *goquery.Selection) {
				interactions = append(interactions, s.Text())
			})
		}
	}

	return interactions
}

func parseDrugInfo(infoUrl string) (drugInfo *DrugInfo, err error) {
	doc, err := getDocumentFromUrl(infoUrl)
	if err != nil {
		log.Fatal(err)
	}

	name := doc.Find("h1").First().Text()

	activeSubstancesText := doc.Find(".drug-subtitle").First().Text()
	// ToDo: split activeSubstancesText into several active substances
	var activeSubstances []string
	if strings.HasPrefix(activeSubstancesText, "Active Substance(s):") {
		activeSubstances = []string{activeSubstancesText[strings.Index(activeSubstancesText, ":")+2:]}
	}

	// ToDo: search for image
	picURL := ""

	var contradictions []string

	sideEffectsDoc, err := getDocumentFromUrl(drugsUrl + "/sfx/" + strings.ToLower(name) + "-side-effects.html")

	var sideEffects []string
	//log.Println(name)
	//log.Println(err)
	if err == nil {
		//log.Println(name)
		afterH3 := sideEffectsDoc.Find("h3").First().NextAll()
		afterH3.Find("li").First().Parent().First().Find("li").Each(func(_ int, s *goquery.Selection) {
			sideEffects = append(sideEffects, s.Text())
		})
	}

	var interactions []string = nil
	interactionsURL, exists := doc.Find("[data-ga-label=\"Interactions\"]").First().Attr("href")
	if exists {
		interactions = parseInteractions(interactionsURL)
	}

	return &DrugInfo{
		Name:             name,
		PicURL:           picURL,
		Contradictions:   contradictions,
		SideEffects:      sideEffects,
		Interactions:     interactions,
		ActiveSubstances: activeSubstances,
	}, nil
}
