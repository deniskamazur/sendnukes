# Go parameters
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
BINARY_API = roza-api

all: clean build	
build:
	$(GOBUILD) -o $(BINARY_API) ./api
clean: 
	$(GOCLEAN)
	rm -f $(BINARY_API)
run: build
	./$(BINARY_API)
test:
	$(GOTEST) ./api