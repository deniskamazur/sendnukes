from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler)
from telegram import (ReplyKeyboardRemove)
import logging


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def name(bot, update):
    user = update.message.from_user
    update.message.reply_text('Ok, now type its name.')
    return 0


def name_saver(bot, update):
    user = update.message.from_user
    logger.info("Drug\'s name of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Thank you! Now ask me what you want.')
    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def handle_mistakes(bot, update):
    update.message.reply_text('Please, send a name or type /cancel')


name_saver_handler = ConversationHandler(
    entry_points=[CommandHandler('name', name)],
    states={
        0: [MessageHandler(Filters.text, name_saver), CommandHandler('cancel', cancel),
            MessageHandler(Filters.all, handle_mistakes)]
    },
    fallbacks=[CommandHandler('cancel', cancel)]
)