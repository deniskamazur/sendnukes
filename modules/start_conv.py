from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler)
from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
import logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def start(bot, update):
    keyboard = list(map(lambda x: ['GMT+' + str(x)], [x for x in range(13)])) + \
               list(map(lambda x: ['GMT-' + str(x)], [x for x in range(1, 13)]))
    update.message.reply_text(
        'Hi! My name is RozalyndBot. I will help you deal with your medications.\n'
        'I can answer your questions about drugs. Also I can notify you when you have to take medication. '
        'Please, tell me about your time zone, so I can notify you correctly, '
        'or type /cancel, if you don\'t want to tell me now.',
        reply_markup=ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True))
    return 0


def timezone(bot, update):
    user = update.message.from_user
    logger.info("Zone of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Thank you! Now send /photo if you want to send a photo of a drug you want to see information for, or send /name if you'
                              'want to type the name of your drug. Also you can type /schedule to set up your schedule.',
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


start_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            0: [MessageHandler(Filters.text, timezone)]
        },
        fallbacks=[CommandHandler('cancel', cancel)]
    )
