from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler)
import logging

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def allergy(bot, update):
    update.message.reply_text('Ok, tell me about your allergies. When you finish, type /cancel')
    return 0


def get_allergy(bot, update):
    user = update.message.from_user
    logger.info("Allergy of %s: %s", user.first_name, update.message.text)
    update.message.reply_text('Thanks, anything else? If not, type /cancel')
    return 0


def cancel(bot, update):
    update.message.reply_text('Thank you! I saved everything.')
    return ConversationHandler.END


allergy_handler = ConversationHandler(
        entry_points=[CommandHandler('allergy', allergy)],
        states={
            0: [MessageHandler(Filters.text, get_allergy)]
        },
        fallbacks=[CommandHandler('cancel', cancel)]
    )
