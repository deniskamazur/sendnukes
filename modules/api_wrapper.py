import requests
from modules.redis_config import db


def get_photo_description(user_id):
    photo = open('{}_photo.jpg'.format(str(user_id)), 'rb')
    data = {'image': photo}
    req = requests.post('http://localhost:8000/api/drugs/getInfo', files=data)
    db.set(user_id, req.json()['name'])
    return req.json()


def get_answer_to_question(question, user_id):
    params = {'question': question,
              'product_name': db.get(user_id)}
    req = requests.get('http://localhost:8000/api/question', params)
    return req.json()
