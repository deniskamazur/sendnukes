from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler)
import logging
import re
from modules.redis_config import db


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def schedule(bot, update):
    update.message.reply_text('Ok, tell me your schedule. When do you need to take your medications?'
                              ' Write in HH:MM format, please. When finish, type /cancel.')
    return 0


def get_time(bot, update):
    user = update.message.from_user
    if re.fullmatch('[0-2][0-9]:[0-6][0-9]', update.message.text):
        logger.info("Alert user %s at %s", user.first_name, update.message.text)
        update.message.reply_text('Thanks, any another time also? If not, type /cancel.')
        chat_id = update.message.chat.id
        try:
            db.set(update.message.text, db.get(update.message.text).append(user.id))
        except:
            db.set(update.message.text, [chat_id])

    else:
        update.message.reply_text('Sorry, you made a mistake in format. '
                                  'Please, type time in format HH:MM, or type /cancel')
    return 0


def cancel(bot, update):
    update.message.reply_text('Thank you!')
    return ConversationHandler.END


def handle_mistakes(bot, update):
    update.message.reply_text('Sorry, you made a mistake in format. '
                              'Please, type time in HH:MM format, or type /cancel.')


time_to_alert_handler = ConversationHandler(
        entry_points=[CommandHandler('schedule', schedule)],
        states={
            0: [MessageHandler(Filters.text, get_time), CommandHandler('cancel', cancel),
                MessageHandler(Filters.all, handle_mistakes)]
        },
        fallbacks=[CommandHandler('cancel', cancel)]
    )
