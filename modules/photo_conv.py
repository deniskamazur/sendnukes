from telegram.ext import (CommandHandler, MessageHandler, Filters, ConversationHandler)
from telegram import (ReplyKeyboardRemove)
import logging
from modules.api_wrapper import get_photo_description

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def photo(bot, update):
    update.message.reply_text('Ok, now send its photo.')
    return 0


def photo_downloader(bot, update):
    user = update.message.from_user
    photo_file = bot.get_file(update.message.photo[-1].file_id)
    photo_file.download('{}_photo.jpg'.format(str(user.id)))
    description = get_photo_description(user.id)
    update.message.reply_text('Thank you very much! This is {0}. It can\'t be used in cases {1}. Some side effects: {2}'
                              'Its active substances: {3}. Now ask me what you want to.'
                              .format(description['name'], description['contradictions'], description['side_effects'],
                                      description['active_substances']))
    return ConversationHandler.END


def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def handle_mistakes(bot, update):
    update.message.reply_text('Please, send a photo or type /cancel')


photo_saver_handler = ConversationHandler(
    entry_points=[CommandHandler('photo', photo)],
    states={
        0: [MessageHandler(Filters.photo, photo_downloader), CommandHandler('cancel', cancel),
            MessageHandler(Filters.all, handle_mistakes)]
    },
    fallbacks=[CommandHandler('cancel', cancel)]
)