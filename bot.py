#!/usr/bin/env python
# -*- coding: utf-8 -*-
import telegram
from telegram.ext import (Updater, MessageHandler, Filters)
from modules.name_conv import name_saver_handler
from modules.photo_conv import photo_saver_handler
from modules.start_conv import start_handler
from modules.allergy_conv import allergy_handler
from modules.schedule_conv import time_to_alert_handler
import logging
import datetime
from threading import Event, Thread
from modules.redis_config import db
from modules.api_wrapper import get_answer_to_question


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
updater = Updater("619811489:AAGJaJly2RWEFqld6ar4vt1cp6gplgxjDbI")

logger = logging.getLogger(__name__)


def set_interval(func, time):
    e = Event()
    while not e.wait(time):
        func()


def send_notification():
    now = str(datetime.datetime.now())[11:16]
    bot = updater.bot
    for i in db.keys():
        if i.decode('utf-8') == now:
                chat_ids = db.get(i).decode('utf-8')
                chat_ids = chat_ids.replace('[', '').replace(']', '').split(', ')
                for chat_id in chat_ids:
                    try:
                        bot.send_message(chat_id, 'It\'s time to get your drugs!')
                    except telegram.error.BadRequest:
                        continue


def text_description(bot, update):
    user = update.message.from_user
    update.message.reply_text(get_answer_to_question(update.message.text, user.id))


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def main():
    # Create the EventHandler and pass it your bot's token.

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    dp.add_handler(start_handler)
    # dp.add_handler(name_saver_handler)
    dp.add_handler(photo_saver_handler)
    dp.add_handler(allergy_handler)
    dp.add_handler(time_to_alert_handler)
    dp.add_handler(MessageHandler(Filters.text, text_description))
    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()


if __name__ == '__main__':
    t1 = Thread(target=set_interval, args=(send_notification, 60))
    t2 = Thread(target=main)
    t1.start()
    t2.start()
    t1.join()
    t2.join()
